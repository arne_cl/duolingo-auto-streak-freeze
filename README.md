duolingo-auto-streak-freeze
===========================

This is a command-line tool to "buy" a streak freeze for Duolingo without
having to use their web interface or app. It can be called as a cronjob,
so you'll never loose your streak as long as you still have enough lingots
in your account.
