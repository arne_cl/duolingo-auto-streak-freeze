#!/usr/bin/env python2.7

import argparse
import datetime
import sys
import duolingo


def cli(argv=sys.argv[1:]):
    parser = argparse.ArgumentParser(
        description='Get a one day streak freeze for a Duolingo account.')

    parser.add_argument(
        '--username', '-u', help='Account username', required=True)

    parser.add_argument(
        '--password', '-p', help='Account password', required=True)

    parser.add_argument(
        '--language', '-l',
        help=('2-letter language code of the language that you need a '
              'streak freeze for'), required=True)

    args = parser.parse_args(argv)

    try:
        duo = duolingo.Duolingo(username=args.username, password=args.password)
        duo.buy_item('streak_freeze', args.language)
        sys.stdout.write(
            "{0} UTC: I bought a streak freeze for {1}\n".format(
                datetime.datetime.utcnow(), args.username))
    except Exception as e:
        sys.stderr.write("{0} UTC: {1}. User name: {2}\n".format(datetime.datetime.utcnow(), e, args.username))
        sys.exit(1)


if __name__ == '__main__':
    cli()
